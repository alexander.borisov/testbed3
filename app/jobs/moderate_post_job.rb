class ModeratePostJob < ApplicationJob
  queue_as :default

  def perform(post)
    sleep 5
    post.moderated!
  end
end
