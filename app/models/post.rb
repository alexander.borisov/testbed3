class Post < ApplicationRecord
  enum :status, [ :unmoderated, :moderated ], default: :unmoderated
end
